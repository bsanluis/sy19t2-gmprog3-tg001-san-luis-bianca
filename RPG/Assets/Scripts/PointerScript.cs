﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PointerScript : MonoBehaviour
{
    public Vector3 moveTarget;
    public PlayerControl PControl;

    public float pointerSpeed;
    public EnemyScript enemy;

    // Start is called before the first frame update
    void Start()
    {
        pointerSpeed = enemy.agent.speed;
    }

    // Update is called once per frame
    void Update()
    {
        MoveTarget();
    }

    void MoveTarget()
    {
        if (PControl.moveTargetSelected == false)
        {
            this.transform.position = new Vector3(-5, -5, -5);
        }

        if (PControl.moveTargetSelected)
        {
            this.transform.position = new Vector3(PControl.moveTarget.x, PControl.moveTarget.y + 0.5f, PControl.moveTarget.z);
        }
    }
}
