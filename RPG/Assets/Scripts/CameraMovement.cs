﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float cameraSpeed = 2.0f;
    public GameObject targetPlayer;
    public Vector3 cameraPosition;

    // Start is called before the first frame update
    void Start()
    {
        cameraPosition = this.transform.position - targetPlayer.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, targetPlayer.transform.position + cameraPosition, cameraSpeed * Time.deltaTime);
        
    }
}
