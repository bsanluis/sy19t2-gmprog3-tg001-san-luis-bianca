﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform playerCamPosition;
    public Transform monsterCamPosition;
    public Transform npcCamPosition;
    public int camPosition;

    void Start()
    {
        this.transform.position = playerCamPosition.position;
        this.transform.rotation = playerCamPosition.rotation;
        
    }

    // Update is called once per frame
    void Update()
    {
        cameraPositionChange(camPosition);
    }

    public void cameraPositionChange(int camPosition)
    {
        switch (camPosition)
        {
            case 1:
                this.transform.position = playerCamPosition.position;
                this.transform.rotation = playerCamPosition.rotation;
                break;
            case 2:
                this.transform.position = monsterCamPosition.position;
                this.transform.rotation = monsterCamPosition.rotation;
                break;
            case 3:
                this.transform.position = npcCamPosition.position;
                this.transform.rotation = npcCamPosition.rotation;
                break;
            default:
                break;
        }
    }
}
