﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private float maxPlayerHP = 100;
    public float playerHP;

    private float baseAtkPower = 10;
    public float atkPower;

    // Start is called before the first frame update
    void Start()
    {
        playerHP = maxPlayerHP;
        atkPower = baseAtkPower;
    }

    //TakeDamage() ?
    //Attack() ?

    // Update is called once per frame
    void Update()
    {

    }
}
