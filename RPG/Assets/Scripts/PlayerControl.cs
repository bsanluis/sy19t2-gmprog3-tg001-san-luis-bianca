﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerControl : MonoBehaviour
{  
    public Vector3 moveTarget;
    public Vector3 enemyTarget;
    public NavMeshAgent agent;
    private CharacterController characterController;

    //public GameObject moveMarker;

    private Animator anim;

    public bool moveTargetSelected = false;
    public bool enemySelected = false;

    // Start is called before the first frame update
    void Start()
    {
        characterController = this.GetComponent<CharacterController>();
        agent = this.GetComponent<NavMeshAgent>();
        moveTarget = this.transform.position;
        enemyTarget = this.transform.position;
        anim = this.GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        SelectMoveTarget();        
    }



    void SelectMoveTarget()
    {
        Ray ray;
        RaycastHit hit;
        Vector3 moveTargetPos;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 hiddenMarkerPos = new Vector3(-5,-5,-5);
        

        if (Physics.Raycast(ray, out hit))
        {
            moveTargetPos = hit.point;
            Vector3 enemyTargetPos = hit.collider.gameObject.transform.position;

            if (Input.GetMouseButtonUp(0))
            {                
                moveTarget = moveTargetPos;
                moveTargetSelected = true;        
            }

            if (agent.remainingDistance == 0 && moveTargetSelected == false)
            {
                moveTargetSelected = false;
            }

            if (moveTargetSelected)
            {               
                //moveMarker.transform.position = new Vector3(hit.point.x, hit.point.y + 0.05f, hit.point.z);
                agent.SetDestination(moveTarget);
                if (agent.remainingDistance <= 0)
                {
                    moveTargetSelected = false;
                }
            }

            //if (targetSelected && enemySelected)
            //{
            //    //moveMarker.transform.position = Vector3.MoveTowards(moveMarker.transform.position, enemyTarget, 1*Time.deltaTime);
            //    agent.SetDestination(enemyTarget);
                
            //}

            //if (agent.remainingDistance <= 1 && enemySelected == true)
            //{
            //    //Face Enemy when Attacking
            //    Vector3 dir = Vector3.RotateTowards(this.transform.forward, enemyTarget, 0.0f, 0.0f);
            //    this.transform.rotation = Quaternion.LookRotation(dir);
            //}

        }            
            Debug.DrawLine(ray.origin, hit.point, Color.red);
        }
       
    }

   //SelectEnemyTarget() Raycast to an enemy on screen
