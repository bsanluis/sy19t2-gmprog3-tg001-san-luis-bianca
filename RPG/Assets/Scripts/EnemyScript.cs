﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    public float maxEnemyHP = 20;
    private float enemyHP;

    public NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        enemyHP = maxEnemyHP;
        agent = this.GetComponent<NavMeshAgent>();
    }

    //TakeDamage() ?
    //Attack() ?

    // Update is called once per frame
    void Update()
    {
        
    }
}
