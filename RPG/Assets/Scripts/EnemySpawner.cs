﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemy;
    public int MaxNumEnemy = 3;
    public float EnemyCount = 0;

    IEnumerator Spawner()
    {
        Vector3 PositionEnemy = new Vector3(this.transform.position.x, 0, this.transform.position.z);
        if (EnemyCount <= MaxNumEnemy)
        {
            GameObject NewEnemy;
            NewEnemy = (GameObject)Instantiate(Enemy, PositionEnemy, Quaternion.identity);
            EnemyCount++;
        }
        yield return new WaitForSeconds(5);
        yield return StartCoroutine(Spawner());
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Spawner());
    }

    // Update is called once per frame
    void Update()
    {
    }
}
