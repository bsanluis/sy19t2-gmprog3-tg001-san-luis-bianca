﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseState : StateMachineBehaviour
{
    public GameObject monster;
    private NavMeshAgent agent;

    private Transform chaseTarget;
    public GameObject player;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        monster = animator.gameObject;
        agent = monster.GetComponent<NavMeshAgent>();
        player = monster.GetComponent<MonsterAI>().GetPlayer();
        chaseTarget = player.transform;

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(chaseTarget.position);


        //CHANGE STATE
        //Debug.Log("CHASE STATE: " + Vector3.Distance(monster.transform.position, player.transform.position));
        float dist = Vector3.Distance(monster.transform.position, player.transform.position);
        if (dist <= 1) //Attack State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", true);
        }
        if (dist >= 5) //Patrol State
        {
            animator.SetBool("isPatrolling", true);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", false);
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
