﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : StateMachineBehaviour
{
    public GameObject monster;
    public GameObject player;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        monster = animator.gameObject;
        player = monster.GetComponent<MonsterAI>().GetPlayer();
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        //CHANGE STATE
        //Debug.Log("IDLE STATE: " + Vector3.Distance(monster.transform.position, player.transform.position));
        float dist = Vector3.Distance(monster.transform.position, player.transform.position);
        if (dist >= 5) //Patrol State
        {
            animator.SetBool("isPatrolling", true);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", false);
        }
        if (dist >= 1 && dist <= 5) //Chase State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", true);
            animator.SetBool("isAttacking", false);
        }
        if (dist <= 1) //Attack State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
