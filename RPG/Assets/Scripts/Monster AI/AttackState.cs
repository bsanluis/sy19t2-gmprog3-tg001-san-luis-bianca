﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackState : StateMachineBehaviour
{
    public GameObject monster;

    private Transform attackTarget;
    public GameObject player;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        monster = animator.gameObject;
        player = monster.GetComponent<MonsterAI>().GetPlayer();
        attackTarget = player.transform;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Look at player while attacking
        Vector3 dir = Vector3.RotateTowards(monster.transform.forward, attackTarget.position, 0.0f, 0.0f);
        monster.transform.rotation = Quaternion.LookRotation(dir);

        //CHANGE STATE
        //Debug.Log("ATTACK STATE: " + Vector3.Distance(monster.transform.position, player.transform.position));
        float dist = Vector3.Distance(monster.transform.position, player.transform.position);
        if (dist >= 5) //Patrol State
        {
            animator.SetBool("isPatrolling", true);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", false);
        }
        if (dist >= 1 && dist <= 5) //Chase State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", true);
            animator.SetBool("isAttacking", false);
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
