﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolState : StateMachineBehaviour
{
    public GameObject monster;
    private NavMeshAgent agent;

    public GameObject player;

    private Vector3 destination;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        monster = animator.gameObject;
        agent = monster.GetComponent<NavMeshAgent>();
        player = monster.GetComponent<MonsterAI>().GetPlayer();

        float destX = monster.transform.position.x + Random.Range(-5, 6);
        float destZ = monster.transform.position.z + Random.Range(-5, 6);
        destination = new Vector3(destX, monster.transform.position.y, destZ);
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        
        agent.SetDestination(destination);

        //CHANGE STATE
        //Debug.Log("PATROL STATE: " + Vector3.Distance(monster.transform.position, destination));
        float distFromPlayer = Vector3.Distance(monster.transform.position, player.transform.position);
        float distFromDest = Vector3.Distance(monster.transform.position, destination);
        if (distFromPlayer >= 1 && distFromPlayer <= 5) //Chase State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", true);
            animator.SetBool("isAttacking", false);
        }
        if (distFromPlayer <= 1) //Attack State
        {
            animator.SetBool("isPatrolling", false);
            animator.SetBool("isChasing", false);
            animator.SetBool("isAttacking", true);
        }
        if (distFromDest <= 1)
        {
            animator.SetBool("isPatrolling", false);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
